# Revising, standardizing, and expanding the Ancient Greek and Latin Dependency Treebank

This is the repository for the website for the homonym DFG project 
(number 408121292)

Related repositories are the ones at:

* https://git.informatik.uni-leipzig.de/celano/ancientgreeknlp
* https://git.informatik.uni-leipzig.de/celano/latinnlp

# Contact
Dr. Giuseppe G. A. Celano<br/>
Universität Leipzig<br/>
Institute of Computer Science, NLP<br/>
Augustusplatz 10<br/>
04109 Leipzig<br/>
Deutschland<br/>
*mysurname* at informatik.uni-leipzig.de<br/>
http://asv.informatik.uni-leipzig.de/staff/Giuseppe_Celano 

# Funder

<a href="http://www.dfg.de/index.jsp" target="_blank">
<img src="https://upload.wikimedia.org/wikipedia/commons/8/86/DFG-logo-blau.svg" 
width="" height="40" alt=""/>
</a>

# Licence

All the data are released under a share-alike licence, which means 
(as the licence specifies) that further data which are created/build upon the 
present data *must* also be released under the same licence. For commercial use,
ask more information.

<a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/">
<img alt="Creative Commons License" style="border-width:0" 
src="https://i.creativecommons.org/l/by-nc/4.0/88x31.png" /></a><br />
This work is licensed under a <a rel="license" 
href="http://creativecommons.org/licenses/by-nc/4.0/">
Creative Commons Attribution-NonCommercial 4.0 International License</a>.
